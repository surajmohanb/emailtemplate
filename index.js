var express = require('express'); 
var routes = require('./routes').routes;
var app = express();

var PORT = 3000;
app.set('view engine', 'ejs');

routes(app);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))