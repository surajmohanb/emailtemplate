exports.routes = function(app) {
    app.get('/', function(req, res){ 
        res.render('../template/simple',{user: "Great User",title:"homepage"});
    });
    app.get('/advanced/:name/:token', function(req, res){ 
        res.render('../template/advanced',{name: req.params.name, token: req.params.token,title:"homepage"});
    });
}